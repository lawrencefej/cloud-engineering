# Docker

## Run Apache docker container
    docker run --name my-apache-app -p 8080:80 httpd:2.4

## Build dockerfile
    docker build -t my-apache .

## Run built docker container
    docker run --name my-apache-app -p 8080:80 my-apache2

## Build dockerfile with version
    docker build -t my-apache:1.0 .

## Run built docker container with version
    docker run --name my-apache-app -p 8080:80 my-apache:1.0


# Docker Compose

# run dokcer compose file
    docker-compose up -d

# stop docker compose
    docker-compose down

# Run docker compose and specifiy the file
    docker-compose -f docker-compose.build.yml up -d

# Docker volume location
    ls /var/lib/docker/volumes

# Run Ubuntu build
    docker build -t my-ubuntu .

# Run Ubuntu built container
    docker run --name my-ubuntu-app -p my-ubuntu
